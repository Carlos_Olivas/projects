`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/04/2016 09:48:01 AM
// Design Name: 
// Module Name: tester_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tester_sim;

    reg clk;
    
    wire [4:0] PC;
    
    tester uut(
        .clk(clk),
        .PC(PC)
    );
        
    initial begin
        #0 clk = 0;
        forever #0.5 clk = ~clk;
    end
     
endmodule
