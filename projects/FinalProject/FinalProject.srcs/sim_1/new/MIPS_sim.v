`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/03/2016 11:24:53 AM
// Design Name: 
// Module Name: MIPS_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MIPS_sim;
    
    reg clock;
    wire [4:0] PC;
    wire [15:0] R1, R2, R3;
    
    MIPS uut(
        .clk(clock),
        .PC(PC),
        .R1(R1),
        .R2(R2),
        .R3(R3)
    );
    
    
    initial begin
        #0 clock = 0;
        forever #1 clock = ~clock;
    end
    
    
endmodule
