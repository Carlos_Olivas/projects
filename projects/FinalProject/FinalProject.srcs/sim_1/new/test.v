`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2016 10:03:25 AM
// Design Name: 
// Module Name: test_mux2to1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test;

// test 2 to 1 mux
//    reg a,b, sel;
//    wire c;
    
//    mux_2to1 uut(
//        .a(a),
//        .b(b),
//        .sel(sel),
//        .out(c)
//    );
    
//      initial begin
//        a = 1'b1;
//        b = 1'b0;
//        sel = 1'b0  ;
//        end  

// test of 16 bit 4 to 1 mux
    // inputs
//    reg [15:0] a; 
//    reg [15:0] b;
//    reg [15:0]c;
//    reg [15:0] d;
//    reg [1:0] sel;
    
//    // outputs
//    wire [15:0] out;
    
//    mux_16bit_4to1 uut(
//        .a(a),
//        .b(b),
//        .c(c),
//        .d(d),
//        .sel(sel),
//        .out(out)
//    );
    
//    initial begin 
//        // initialize inputs
//        a = { 15'b0, 1} ; //16'd15;
//        b = 16'd32;
//        c = 16'd20;
//        d = 16'd6;
//        sel = 2'b00;
//    end
    
// test of 16 bit adder
//     inputs
//    reg [15:0] a;
//    reg [15:0] b;
//    reg cin;
    
//    //outputs
//    wire [15:0] sum;
//    wire cout;
//    wire [15:0] test;
    
    
//    adder_16Bit uut(
//        .a(a),
//        .b(b),
//        .cin(cin),
//        .sum(sum),
//        .cout(cout)
//    );
    
//    initial begin
//        a = 16'd20;
//        b = 16'd30;
//        cin = 1;
         
//    end
    
// test instruction memory
    // inputs
//    reg [5:0] a;
    
//    // output
//    wire [15:0] instruction;
    
//    instructionMemory uut(
//        .pc(a),
//        .instruction(instruction)
//    );
    
//    initial begin
//        a = 5'b00010;
        
//    end
    
//    reg [15:0] q,w;
//    wire [15:0] z;
    
//    tester uut(
//        .q(q),
//        .w(w),
//        .z(z)
//    );
    
//    initial begin
//        q = 16'b0000000000000000;
//        w = 16'b1111111111111111;
//    end 

    reg clk; 
    reg [4:0] oldCount;
    wire [4:0] newCount;
    
    pc uut(
        .clk(clk),
        .oldCount(oldCount),
        .newCount(newCount)
    );

    initial begin
        clk =  0; 
        forever #1 clk = ~clk;
        
    end
    
    initial begin
        oldCount = 0;
        
        #2 oldCount = 1;
        #2 oldCount =2 ;
        
    end
    
    

endmodule
