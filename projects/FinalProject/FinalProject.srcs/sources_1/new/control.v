`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/03/2016 01:05:34 AM
// Design Name: 
// Module Name: control
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module control(
    input [3:0] opcode,
    output reg BNE,
    output reg SLT
    );
    
    always @(opcode)
    begin
        if(opcode == 8)
        begin
            BNE = 1;
            SLT = 0;
        end
        
        else if(opcode == 12)
        begin
            BNE = 0;
            SLT = 1;
        end
        else if(opcode == 15)
        begin
            BNE = 1;
            SLT = 1;
        end
        else
        begin
            BNE = 0;
            SLT = 0;
        end
        
    end
    
        
    
endmodule
