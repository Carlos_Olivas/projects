`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/02/2016 06:21:58 PM
// Design Name: 
// Module Name: pc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pc(
    input clk,
    input [4:0] oldCount,
    output reg [4:0] newCount
    );
    
    reg [4:0] tmp = 0;
    
    always @(clk)
    begin
        
        if(oldCount)
           assign newCount = oldCount + 1'b1;
        else
            assign newCount = 1;
    end
                
endmodule
