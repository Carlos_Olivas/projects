`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2016 11:48:47 AM
// Design Name: 
// Module Name: mux_16bit_2to1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux_16bit_2to1(
    input [15:0] a,
    input [15:0] b,
    input sel,
    output [15:0] out
    );
    
    mux_2to1 inst[15:0] (a, b, sel, out);
    
endmodule
