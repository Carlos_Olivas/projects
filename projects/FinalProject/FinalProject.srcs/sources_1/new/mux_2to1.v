`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2016 09:22:56 AM
// Design Name: 
// Module Name: mux_2to1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux_2to1(
    input a,
    input b,
    input sel,
    output out
    );
    
    wire t0, t1;
    
    assign t0 = ~sel & a;
    assign t1 = sel & b;
    assign out = t0 | t1;
//    assign out = (sel) ? b : a;
    
endmodule
