`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2016 03:58:34 AM
// Design Name: 
// Module Name: 16bit_Adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module adder_16Bit(
      input [15:0] a,
      input [15:0] b,
      input cin, 
      output [15:0] sum, 
      output cout 
    );
    
    //  for subtracting, 2's complement b input
    wire [15:0] b2 = b ^{ {15{cin}}, cin};
          
    fullAdder a0(.a(a[0]), .b(b2[0]), .cin(cin), .sum(sum[0]), .cout(c0));
    fullAdder a1(.a(a[1]), .b(b2[1]), .cin(c0), .sum(sum[1]), .cout(c1));
    fullAdder a2(.a(a[2]), .b(b2[2]), .cin(c1), .sum(sum[2]), .cout(c2));
    fullAdder a3(.a(a[3]), .b(b2[3]), .cin(c2), .sum(sum[3]), .cout(c3));
    fullAdder a4(.a(a[4]), .b(b2[4]), .cin(c3), .sum(sum[4]), .cout(c4));
    fullAdder a5(.a(a[5]), .b(b2[5]), .cin(c4), .sum(sum[5]), .cout(c5));
    fullAdder a6(.a(a[6]), .b(b2[6]), .cin(c5), .sum(sum[6]), .cout(c6));
    fullAdder a7(.a(a[7]), .b(b2[7]), .cin(c6), .sum(sum[7]), .cout(c7));
    fullAdder a8(.a(a[8]), .b(b2[8]), .cin(c7), .sum(sum[8]), .cout(c8));
    fullAdder a9(.a(a[9]), .b(b2[9]), .cin(c8), .sum(sum[9]), .cout(c9));
    fullAdder a10(.a(a[10]), .b(b2[10]), .cin(c9), .sum(sum[10]), .cout(c10));
    fullAdder a11(.a(a[11]), .b(b2[11]), .cin(c10), .sum(sum[11]), .cout(c11));
    fullAdder a12(.a(a[12]), .b(b2[12]), .cin(c11), .sum(sum[12]), .cout(c12));
    fullAdder a13(.a(a[13]), .b(b2[13]), .cin(c12), .sum(sum[13]), .cout(c13));
    fullAdder a14(.a(a[14]), .b(b2[14]), .cin(c13), .sum(sum[14]), .cout(c14));
    fullAdder a15(.a(a[15]), .b(b2[15]), .cin(c14), .sum(sum[15]), .cout(cout));
   
  
    
endmodule
