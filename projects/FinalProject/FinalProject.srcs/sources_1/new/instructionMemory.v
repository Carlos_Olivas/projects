`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/29/2016 11:11:36 PM
// Design Name: 
// Module Name: instructionMemory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module instructionMemory(
    input [4:0] pc,
    output [15:0] instruction
    );
    
    reg [0:15] ins[14:0];
    
    initial begin
        ins[0] = 16'd0;
        ins[1] = 16'b0010001001000100;
        ins[2] = 16'b0110001100010011;
        ins[3] = 16'b1000001100000000;
    end
    
    assign instruction = ins[pc];
    
    
    
endmodule
