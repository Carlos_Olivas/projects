`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/01/2016 10:53:50 PM
// Design Name: 
// Module Name: tester
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tester(
    input clk,
    output [4:0] PC
    );
    
    reg [4:0] oldCount;
    wire [4:0] currentCount;
    wire [4:0] tmp;
    
    pc ProgramCounter (.clk(clk), .oldCount(oldCount), .newCount(currentCount));  
    
    mux_5bit_2to1 Mux1(.a(currentCount), .b(5'b00000), .sel(1'b0), .out(tmp));
   
   always @(posedge clk)
   begin
        oldCount = tmp;
   end
    
    assign PC = currentCount;    
endmodule
