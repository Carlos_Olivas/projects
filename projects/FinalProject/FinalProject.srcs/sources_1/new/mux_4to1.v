`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2016 09:34:00 AM
// Design Name: 
// Module Name: mux_4to1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux_4to1(
    input a, b, c, d,
    input [1:0] sel,
    output out
    );
    
    wire t0, t1;
  
    assign t1 = ~( (sel[1] & d) | (~sel[1] & b) );
    assign t0 = ~( (sel[1] & c) | (~sel[1] & a) );    
    assign out = ~( (t0 | sel[0]) & (t1 | ~sel[0]) );

endmodule
