`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/02/2016 06:19:17 PM
// Design Name: 
// Module Name: MIPS
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MIPS(
    input clk,
    output [4:0] PC,
    output [15:0] R1, R2, R3
    );
    
    wire [15:0] instruction;
    reg [4:0] oldCount;
    wire [4:0] currentCount;
    wire [4:0] tmpCount;
    wire [15:0] Cdata;
    wire [15:0] rs;
    wire [15:0] rt;

    
    pc ProgramCounter (.clk(clk), .oldCount(oldCount), .newCount(currentCount));   
    
    instructionMemory InstructionMemory(.pc(currentCount), .instruction(instruction));
    
    control Control(.opcode(instruction[15:12]), .BNE(BNE), .SLT(SLT));
    
    registerFile RegisterFile(.Aaddr(instruction[11:8]), .Baddr(instruction[7:4]), .Caddr(instruction[3:0]), 
            .C(Cdata), .load(load), .clear(), .clock(clk), .A(rs), .B(rt), .R1(R1), .R2(R2), .R3(R3));
    
    alu ALU(.X(rs), .Y(rt), .Cin(instruction[2]), .opcode(instruction[15:12]), .out(Cdata), .cout(), .lt(lt), .eq(eq), .gt(), .V() );
    
    mux_4to1 Mux1(.a(1'b1), .b(lt), .c(1'b0), .d(1'b0), .sel({BNE, SLT}), .out(load));
    
    mux_5bit_2to1 Mux2(.a(currentCount), .b({1'b0, instruction[3:0]}), .sel( (BNE& (~eq)) ), .out(tmpCount));
    
    always @(posedge clk) 
    begin

        oldCount = tmpCount;
    end
    
    assign PC = currentCount;

    
endmodule
