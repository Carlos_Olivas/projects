`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2016 07:02:50 PM
// Design Name: 
// Module Name: ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module alu(
    input [15:0] X,
    input [15:0] Y, 
    input Cin, 
    input [3:0] opcode,
    output [15:0] out,
    output cout, 
    output lt, eq, gt, V  
    );
    
    // opcodes
    //  add: 0010 
    //  sub: 0110
    //  and: 0000
    //  or:  0001
    //  slt: 1100
    //  bne: 1000
    
    wire [15:0] t1;     // temp for adder output
    wire [15:0] t2;     // temp for AND or OR
    wire [15:0] t3;     // temp for outcome of X - Y
    wire [15:0] t4;
    wire [15:0] t5; 
    
    adder_16Bit Addition(.a(X), .b(Y), .cin(opcode[2]), .sum(t1), .cout(cout));     // for Add and Subtract
    mux_16bit_2to1 Mux1(.a((X&Y)), .b((X|Y)), .sel(opcode[0]), .out(t2));       // for AND and OR  
    adder_16Bit XminusY(.a(X), .b(Y), .cin(1'b1), .sum(t3), .cout(cout));     // subtract Y from X to see if equal, t3 should be 0
    
    mux_16bit_2to1 Mux2(.a(t2), .b(t1), .sel(opcode[1]), .out(t4));     // decide output between (AND and OR) and (ADD and SUB) 
    
    mux_16bit_2to1 Mux3(.a(t4), .b({15'b0, t3[15]}), .sel(opcode[3]), .out(t5));     // decide output between above and STL

    assign out = t5;
    assign V = cout;
    assign lt = t3[15];
    assign gt = t3[15];
    assign eq = ~(t3[0] | t3[1] | t3[2] | t3[3] | t3[4] | t3[5] | t3[6] | 
        t3[7] | t3[8] | t3[9] | t3[10] | t3[11] | t3[12] | t3[13] | t3[14] | t3[15]);
    
    
endmodule
