`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/03/2016 01:18:02 AM
// Design Name: 
// Module Name: mux_5bit_2to1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux_5bit_2to1(
    input [4:0] a,
    input [4:0] b,
    input sel,
    output [4:0] out
    );
    
    mux_2to1 inst[4:0] (a, b, sel, out);
        
endmodule
