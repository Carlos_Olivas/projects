`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/29/2016 11:09:53 PM
// Design Name: 
// Module Name: registerFile
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module registerFile(
    input [3:0] Aaddr,
    input [3:0] Baddr,
    input [3:0] Caddr,
    input [15:0] C,
    input load,
    input clear,
    input clock,
    output [15:0] A,
    output [15:0] B,
    output [15:0] R1, R2, R3
    );
    
    reg [15:0] data[0:15];
    
    assign A = data[Aaddr];
    assign B = data[Baddr];
    
    initial begin 
        data[0] = 16'd0;    // 0
        data[1] = 16'd1;    // 1
        data[2] = 16'd4;    // $R1
        data[3] = 16'd6;    // $R2
        data[4] = 16'd0;    // $R3        
        data[5] = 16'd0;    // t1
        data[6] = 16'd0;    // t2
        data[7] = 16'd0;
        data[8] = 16'd0;
        data[9] = 16'd0;
        data[10] = 16'd0;
        data[11] = 16'd0;
        data[12] = 16'd0;
        data[13] = 16'd0;
        data[14] = 16'd0;
        data[15] = 16'd0;
    end
    
    always @(posedge clock)
    begin
        if(load)
            data[Caddr] <= C;
    end
    
    assign R1 = data[2];
    assign R2 = data[3];
    assign R3 = data[4];

endmodule
