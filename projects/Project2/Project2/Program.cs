﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please input file path. Make sure file is in bin folder and path begins with '..\\\\' ");
            string path = Console.ReadLine();

            Console.WriteLine("Please indicate the number of selected output: ");
            int numOutputs = Int32.Parse(Console.ReadLine());
         
            string[] selOutputs = new string[numOutputs];
            Console.WriteLine("Please input the selected outputs, make sure not to type an extra space");
            for(int i = 0; i < numOutputs; i++)
            {
                Console.WriteLine("Output " + (i+1) + ": ");
                selOutputs[i] = Console.ReadLine();
            }

            for(int i=0; i< numOutputs; i++)
            {
                selOutputs[i] = selOutputs[i].ToUpper(); // make the selected outputs uppercase to facilitate later comparisons
            }


            string[] fileLines = new string[50];  // array to hold the lines read from the file, max 50 lines because max 50 gates
            int lineCount = 0;  // used to keep count of number of lines, which is also the number of gates
            LogicGate[] gates;
            int gateCount = 0;
            
            using (StreamReader reader = File.OpenText(path))       // Read the lines from the file
            {
                string temp;

                while (!reader.EndOfStream)
                {
                    temp = reader.ReadLine();
                    if (!string.IsNullOrEmpty(temp))
                    {
                        fileLines[lineCount] = temp;    // store each line of the file
                        lineCount++;        // count how many lines are in the file
                    }
                }


            }

            if(lineCount > 50)  // check that the max number of gates is not exceeded
            {
                Console.WriteLine("The specified file indicates too many gates. Please select another file that indicates less then 50 gates.");
                return;
            }
            else
            {
                gates = new LogicGate[lineCount]; // set the number of gates for the array of the logic gates of the circuit

                for (int i=0; i<lineCount; i++)
                {
                    fileLines[i] = fileLines[i].ToUpper();    
                    string[] gateComp = fileLines[i].Split(' ');    // split the line into the gate components

                    // check that the limitations are not exceeded
                    if(gateComp[1].Length > 12)
                    {
                        Console.WriteLine("One of the gate names exceeds 12 characters. Please check revise.");
                        return;
                    }
                    if(gateComp[2].Length > 5)
                    {
                        Console.WriteLine("One of the gate types exceeds 5 characters, and so cannot match any gate type. Please revise.");
                        return;
                    }
                    if((gateComp.Length -3) > 8)
                    {
                        Console.WriteLine("One of the gates has more than 8 inputs. Please revise.");
                        return;
                    }

                    gates[gateCount] = new LogicGate(gateComp);    // initialize a gate 
                    gateCount++;
                }
                
            }
            Circuit theCircuit = new Circuit(gates, selOutputs);    // initialize the circuit
            theCircuit.perfomCircuit();     // perform the circuit logic
            theCircuit.circuitListing();    // print out the results
        }
    }
}
