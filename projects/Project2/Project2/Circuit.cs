﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    class Circuit
    {
        public LogicGate[] circuitGates;
        public string[] selectedOutput;
        public int[,] bitInputs;
        public double numPermIn;
        public char[] inputs;
        public int[,] outputs;

        public Circuit(LogicGate[] gates, string[] selOut)   
        {
            circuitGates = gates;
            selectedOutput = selOut;
        }

        public int findIndex(char letter)   // find the index of the letter
        {
            char[] alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

            for (int i = 0; i < 26; i++)
            {
                if (letter == alpha[i])
                    return i;
            }

            return -1;
        }

        public void sortCircuitGates()  // sort the array of LogicGates of the circuit by gate number
        {
            LogicGate tempGate;

            for (int i=0; i < circuitGates.Length; i++)
            {
                if (circuitGates[i].gateNumber != i+1)
                {
                    tempGate = circuitGates[i];

                    for (int j = 0; j < circuitGates.Length; j++)
                    {
                        if (circuitGates[j].gateNumber == i+1)
                        {
                            circuitGates[i] = circuitGates[j];
                            circuitGates[j] = tempGate;
                        }
                    }
                }    
            }
        }

        public void circuitListing()    // print out the circuit listing
        {
            Console.WriteLine("");
            Console.WriteLine("Circuit Listing");
            Console.WriteLine("---------------");
            for (int i =0; i < circuitGates.Length; i++)
            {
                if (circuitGates[i].gateNumber == i+1)
                {
                    Console.Write(circuitGates[i].gateNumber + "      "+circuitGates[i].gateName+ "   "+ circuitGates[i].gateType+ "     ");
                }

                for (int j = 0; j < circuitGates[i].inputs.Length; j++)
                {
                    Console.Write(circuitGates[i].inputs[j] + " ");
                }
                Console.WriteLine("");
            }

            Console.WriteLine();
            Console.WriteLine("Truth Table for the Selected Outputs");
            Console.WriteLine("------------------------------------");
            for (int k = 0; k < inputs.Length; k++)     // print out the letter inputs
            {
                Console.Write(inputs[k] + "     ");
            }

            for (int l = 0; l < selectedOutput.Length; l++)     // print out the selected outputs
            {
                Console.Write(selectedOutput[l]+"    ");
            }
            Console.WriteLine("");

            for (int x = 0; x < numPermIn; x++)     // print out the truth table, go line by line
            {
                for (int y = 0; y < inputs.Length; y++)     // print out the bits of the inputs
                {
                    Console.Write(bitInputs[x,y] + "     ");
                }

                for (int w = 0; w < selectedOutput.Length; w++) // print out the bits of the output
                {
                    Console.Write(outputs[x,w]+ "     ");
                }

                Console.WriteLine("");
            }

        }

        public void subConstructor()
        {
            int numDiffInputs = 0;

            // find the number of distinct letter inputs
            char[] letterCompare = new char[26];
            for (int i = 0; i < circuitGates.Length; i++)   // go through each gate
            {
                for (int j = 0; j < circuitGates[i].inputs.Length; j++) // go through each input of the gate
                {
                    if (Char.IsLetter(circuitGates[i].inputs[j]))   // check if the input is a letter
                    {
                        int counter =0;
                        for (counter = 0; counter < circuitGates.Length; counter++)
                        {
                            if (circuitGates[i].inputs[j] == letterCompare[counter])  // if letter has already been accounted for
                                break;
                        }
                        if (counter == circuitGates.Length)  // if letter is new
                        {
                            letterCompare[numDiffInputs] = circuitGates[i].inputs[j];   // add the letter to the letter array for comparison
                            numDiffInputs++;
                        }
                    }     
                }
            }

            numPermIn = Math.Pow(2, numDiffInputs); // find the different input permutations

            bitInputs = new int[Convert.ToInt32(numPermIn), numDiffInputs];  // will hold the values for the inputs

            for (int k = 0; k < numPermIn; k++)     // seperate into bits
            {
                string permIn = Convert.ToString(k, 2); // convert to bits of base 2
                if (permIn.Length < numDiffInputs)
                {
                    permIn = permIn.PadLeft(numDiffInputs, '0');    // make sure there is a bit for each input
                }

                char[] tmp = permIn.ToCharArray();  // temporarily hold the array of bits

                for (int l = 0; l < numDiffInputs; l++)
                {
                    bitInputs[k, l] = Convert.ToInt32(Char.GetNumericValue(tmp[l]))   ;   // convert to int and store into array
                }
            }

            
            int diffCounter = 0;
            char[] temp = "..........................".ToCharArray();
            for (int y = 0; y < letterCompare.Length; y++)
            {
                if (Char.IsLetter(letterCompare[y]))   // if the input is a letter, find the index of it in the truth table
                {
                    int index = findIndex(letterCompare[y]);
                    temp[index] = letterCompare[y];
                    diffCounter++;
                }
            }

            inputs = new char[diffCounter];
            for (int x = 0; x < diffCounter; x++)
            {
                inputs[x] = temp[x];
            }
            
        }

        public void perfomCircuit()     // perform the truth table for the selected outputs
        {
            subConstructor();
            sortCircuitGates();

            outputs = new int[Convert.ToInt32(numPermIn),selectedOutput.Length];

            for (int x = 0; x < numPermIn; x++) // go through each line of truth table
            {
                int[] tempResult = new int[circuitGates.Length];    //

                for (int y = 0; y < circuitGates.Length; y++)   // go through each gate
                {
                    int[] gateLogicInputs = new int[ circuitGates[y].inputs.Length];  // to be passed to the gate's logic function 

                    for (int w = 0; w < circuitGates[y].inputs.Length; w++)    // go through each input of the gate
                    {
                        if (Char.IsLetter(circuitGates[y].inputs[w]))   // if the input is a letter, find the index of it in the truth table
                        {
                            int index = findIndex(circuitGates[y].inputs[w]);
                            gateLogicInputs[w] = bitInputs[x,index]; // assign it as the corresponding bit value form the truth table to the array for the gate logic
                        }
                        else
                        {
                            for (int q = 0; q < circuitGates.Length; q++)
                            {
                                if (circuitGates[q].gateNumber == Char.GetNumericValue(circuitGates[y].inputs[w]))    // if the input is a number, find the logic output of the gate with the 
                                {                                                                               //  corresponding gate number
                                    gateLogicInputs[w] = circuitGates[q].logicResult;
                                }
                            }
                            
                        }
                    }

                    circuitGates[y].logicFunction(gateLogicInputs);     // perform the gate's logic

                    for (int z = 0; z < selectedOutput.Length; z++)
                    {
                        if (circuitGates[y].gateName == selectedOutput[z])
                        {
                            outputs[x,z] = circuitGates[y].logicResult;   // if the gate is a selected output, place in outputs array
                        }
                    }
                    
                }
            }

        }

        

        ~Circuit() { }
    }
}
