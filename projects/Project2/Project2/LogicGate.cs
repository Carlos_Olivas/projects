﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    class LogicGate
    {
        public int gateNumber;
        public string gateName;
        public string gateType;
        public char[] inputs;
        public int logicResult;

       public LogicGate(string[] gateComp) // initialize the logic gate
        {

            // check for any empty cells and remove them
            int emptyCounter=0;
            for (int i = 0; i < gateComp.Length; i++)
            {
                if (string.IsNullOrEmpty(gateComp[i]))  // find empty cell
                {
                    gateComp = gateComp.Where(x => x != gateComp[i]).ToArray(); // remove it from the array

                }
            }

            gateNumber = Int32.Parse(gateComp[0]);
            gateName = gateComp[1];
            gateType = gateComp[2];

            int inputNum = gateComp.Length - 3; // find the number of inputs for the gate

            inputs = new char[inputNum];     // allocate space for the specified number of inputs

            for (int i = 0; i < inputNum; i++)  // fill the gate's input array
            {
                string tmp = gateComp[3+i];               
                inputs[i] = Convert.ToChar(tmp);
            }
        }

        

        public void logicFunction(int[] theInputs)
        {
            switch (gateType)
            {
                case "NOT":
                {
                    for (int i = 0; i < theInputs.Length; i++)
                    {
                        if (theInputs[i] == 0)
                        {
                            logicResult = 1;
                        }
                        else if (theInputs[i] == 1)
                        {
                            logicResult = 0;
                        }
                    }
                    break;
                }
                case "AND":
                {
                    int counter = 0;
                    int temp = -1;
                    for (int i = 0; i < theInputs.Length; i++)
                    {
                        if (counter == 0)
                        {
                            temp = theInputs[i];
                            counter++;
                        }
                        else
                        {
                            temp = temp & theInputs[i];
                        }
                    }
                    logicResult = temp;
                    break;
                }
                case "OR":
                {
                    int counter = 0;
                    int temp = -1;
                    for (int i = 0; i < theInputs.Length; i++)
                    {
                        if (counter == 0)
                        {
                            temp = theInputs[i];
                            counter++;
                        }
                        else
                        {
                            temp = temp | theInputs[i];
                        }
                    }
                    logicResult = temp;
                    break;
                }
                case "XOR":
                {
                    int counter = 0;
                    int temp = -1;
                    for (int i = 0; i < theInputs.Length; i++)
                    {
                        if (counter == 0)
                        {
                            temp = theInputs[i];
                            counter++;
                        }
                        else
                        {
                            temp = temp ^= theInputs[i];
                        }
                    }
                    logicResult = temp;
                    break;
                }
                case "NAND":
                {
                    int counter = 0;
                    int temp = -1;
                    for (int i = 0; i < theInputs.Length; i++)
                    {
                        if (counter == 0)
                        {
                            temp = theInputs[i];
                            counter++;
                        }
                        else
                        {
                            temp = (temp & theInputs[i]);
                        }
                    }
                    if (temp == 0)
                    {
                        logicResult = 1;
                    }
                    else if (temp == 1)
                    {
                        logicResult = 0;
                    }
                    break;
                }
                case "NOR":
                {
                    int counter = 0;
                    int temp = -1;
                    for (int i = 0; i < theInputs.Length; i++)
                    {
                        if (counter == 0)
                        {
                            temp = theInputs[i];
                            counter++;
                        }
                        else
                        {
                            temp = temp | theInputs[i];
                        }
                    }
                    if (temp == 0)
                    {
                        logicResult = 1;
                    }
                    else if (temp == 1)
                    {
                        logicResult = 0;
                    }
                    break;
                }
                case "XNOR":
                {
                    int counter = 0;
                    int temp = -1;
                    for (int i = 0; i < theInputs.Length; i++)
                    {
                        if (counter == 0)
                        {
                            temp = theInputs[i];
                            counter++;
                        }
                        else
                        {
                            temp = temp ^= theInputs[i];
                        }
                    }
                    if (temp == 0)
                    {
                        logicResult = 1;
                    }
                    else if (temp == 1)
                    {
                        logicResult = 0;
                    }
                    break;
                }

            }
        }

        ~LogicGate() { }
    }
}
